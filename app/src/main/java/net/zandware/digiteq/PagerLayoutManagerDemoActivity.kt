package net.zandware.digiteq

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import java.util.Random

class PagerLayoutManagerDemoActivity : AppCompatActivity() {
    class DemoViewHolder(context: Context?) : RecyclerView.ViewHolder(TextView(context)) {
        val textView: TextView

        init {
            textView = itemView as TextView
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var demoRclr = findViewById<RecyclerView>(R.id.demo_rclr)
        demoRclr.adapter =  object : RecyclerView.Adapter<DemoViewHolder>() {
            private val random = Random()
            private val strings: List<String>? = listOf(
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"
            )

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): DemoViewHolder {
                return DemoViewHolder(this@PagerLayoutManagerDemoActivity)
            }

            override fun onBindViewHolder(demoViewHolder: DemoViewHolder, position: Int) {
                demoViewHolder.textView.text = strings!![position]
                demoViewHolder.textView.setBackgroundColor(0x88 or random.nextInt())
            }

            override fun getItemCount(): Int {
                return strings?.size ?: 0
            }
        }
        val pagerLayoutManager = PagerLayoutManager(3, 5, false)
        demoRclr.setLayoutManager(pagerLayoutManager)
        PagerLayoutManager.SnapHelper().attachToRecyclerView(demoRclr)
    }
}