package net.zandware.digiteq

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class PagerLayoutManager(columns: Int, rows: Int, reversed: Boolean) :
    RecyclerView.LayoutManager() {
    val columns: Int
    val rows: Int
    val reversed: Boolean

    init {
        if (columns <= 0) {
            throw RuntimeException("columns must be greater than 0")
        }
        this.columns = columns
        if (rows <= 0) {
            throw RuntimeException("rows must be greater than 0")
        }
        this.rows = rows
        this.reversed = reversed
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        removeAndRecycleAllViews(recycler)
        val pages = itemCount / columns / rows
        val columnWidth = width / columns.toFloat()
        val rowHeight = height / rows.toFloat()
        for (i in 0 until itemCount) {
            // pages and grid coordinates
            var column = i % columns
            val row = i % (columns * rows) / columns
            var page = i / (columns * rows)
            if (reversed) {
                column = columns - column - 1
                page = pages - page
            }

            // item position
            val x = (column + page * columns) * columnWidth
            val y = row * rowHeight

            // view coordinates
            val left = (x - scrollX).toInt()
            val top = y.toInt()
            val right = (left + columnWidth).toInt()
            val bottom = (top + rowHeight).toInt()
            val childView = recycler.getViewForPosition(i)
            addView(childView)
            layoutDecorated(childView, left, top, right, bottom)
        }
    }

    var scrollX = 0

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State
    ): Int {
        val minScrollX = 0
        val maxScrollX = width * (itemCount / columns / rows)
        val newScrollX = Math.max(minScrollX, Math.min(scrollX + dx, maxScrollX))
        val actualDx = newScrollX - scrollX
        scrollX = newScrollX
        offsetChildrenHorizontal(-actualDx)
        return actualDx
    }

    override fun canScrollHorizontally(): Boolean {
        return true
    }

    override fun canScrollVertically(): Boolean {
        return false
    }

    override fun onLayoutCompleted(state: RecyclerView.State) {
        super.onLayoutCompleted(state)
    }

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(width / columns, height / rows)
    }

    class SnapHelper : androidx.recyclerview.widget.SnapHelper() {
        override fun calculateDistanceToFinalSnap(
            layoutManager: RecyclerView.LayoutManager,
            targetView: View
        ): IntArray? {
            val pagerLayoutManager = layoutManager as PagerLayoutManager
            val scrollX = layoutManager.scrollX
            val targetLeft = targetView.left + scrollX
            val distanceToSnapX = targetLeft - scrollX
            return intArrayOf(distanceToSnapX, 0)
        }

        override fun findSnapView(layoutManager: RecyclerView.LayoutManager): View? {
            val pagerLayoutManager = layoutManager as PagerLayoutManager
            val scrollX = pagerLayoutManager.scrollX
            val width = pagerLayoutManager.width
            if (width == 0) {
                return null
            }
            val page = (scrollX + width / 2) / width
            val pageLeft = page * width
            for (i in 0 until layoutManager.getChildCount()) {
                val child = layoutManager.getChildAt(i)
                val childLeft = child!!.left + scrollX
                if (childLeft == pageLeft) {
                    return child
                }
            }
            return null
        }

        override fun findTargetSnapPosition(
            layoutManager: RecyclerView.LayoutManager,
            velocityX: Int,
            velocityY: Int
        ): Int {
            return 0
        }
    }
}